function getYear(item) {
    const date = item.date;
    return date.split('-')[0];
}

// data is JSON returned from the backend
function process_timelines(data) {
    var current_year = "";
    var year = "";
    var current_id = "";
    var id = "";

    for (let i=0; i<data.length; i++) {
	year = getYear(data[i]);
        id = data[i]._id;
	
	if (year === current_year) {
	    data[i]["disp_year"] = "";
	} else {
	    current_year = year;
	    data[i]["disp_year"] = current_year;
	}

	data[i]["repeat"] = false;
	data[i]["remove"] = false;
        if (id === current_id) {
            data[i-1]["repeat"] = true;
	    data[i]["remove"] = true;
	} else {
	    current_id = id;
	}
    }

    return data.filter(item => {return !item["remove"]});
}

export default process_timelines;
