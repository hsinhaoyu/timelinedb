import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import MainPanel from "./MainPanel";
import AboutPanel from "./AboutPanel";
import LoginPanel from "./LoginPanel";

export default function MenuForUser(props) {
    const allTagsState = props.allTagsState;

    const [tabState, setTabState] = React.useState(0);

    const handleTabChange = (event, newValue) => {
        setTabState(newValue);
    };
    
    return (
        <Box sx={{ width: '100%'}}>
            <Tabs
	        value = {tabState}
	        onChange = {handleTabChange}>
                <Tab disableRipple label = "Home" />
	        <Tab disableRipple label = "About" />
                <Tab disableRipple label = "Login" />
            </Tabs>

	    <div
	        hidden = {tabState !== 0}
                id = 'pane0'>
	        {tabState === 0 && (<MainPanel
				        allTagsState = {allTagsState} 
                                        isLoggedIn = {props.isLoggedIn} />)}
            </div>

	    <div
	        hidden = {tabState !== 1}
                id = 'pane1'>
		{tabState === 1 && (<AboutPanel />)}
            </div>

	    <div
	        hidden = {tabState !== 2}
                id = 'pane2'>
		{tabState === 2 && (<LoginPanel />)}
            </div>
        </Box>
  );
}
