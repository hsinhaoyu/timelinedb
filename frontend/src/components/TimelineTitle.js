import * as React from 'react';
import Box from '@mui/material/Box';

import RemoveTimelineButton from "./RemoveTimelineButton";
import AddTimelineButton from "./AddTimelineButton";

// i: the index of the timeline (1 or 2)
function TimelineTitle(props) {

    const n = props.selTags.length;

    return (
        <Box sx={{display: 'flex', flexDirection: 'row', width: '120px', alignSelf: 'flex-end', alignItems: 'center'}}>

            {n === 2 ?
	     <RemoveTimelineButton
		 i = {props.i}
		 selTags = {props.selTags}
		 setSelTags = {props.setSelTags} /> :
	     <AddTimelineButton
	         selTags = {props.selTags}
		 setSelTags = {props.setSelTags} />
	    }

	    <Box sx={{alignItems: 'baseline', fontFamily: "Special Elite", fontSize: 14}}>
                Timeline {props.i}
	    </Box>
	</Box>
    )
}

export default TimelineTitle;
