import React from "react";

import MenuItem from "@mui/material/MenuItem";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

export default function ExcQueryPrompt(props) {

    return (
        <Box sx={{display: 'flex'}}>
	
            <Box sx={{display: 'flex', alignItems: 'center', fontSize: 14, fontFamily: 'Special Elite', p:0, m:0}}>but exclude those with</Box>
	  
            <FormControl
	        variant = "standard"
	        sx = {{ml:1, p:0, minWidth: 50}}
	    >	    
            <Select
	        name = {props.name}
                sx={{fontSize: 14, fontFamily: 'Special Elite'}}
                value={props.op}
                onChange={(e, v) => {props.updateSelTags(e, v.props.value, props.i)}}
                displayEmpty
	        disableUnderline>
		
            <MenuItem sx={{pl:1, pt:0, pb:0, fontSize: 14, fontFamily: 'Special Elite'}} value="all">all</MenuItem>
            <MenuItem sx={{pl:1, pt:0, pb:0, fontSize: 14, fontFamily: 'Special Elite'}} value="any">any</MenuItem>

            </Select>
            </FormControl>
	    
            <Box sx={{display: 'flex', alignItems: 'center', fontSize: 14, fontFamily: 'Special Elite'}}>of these tags (if blank, exclude nothing)</Box>

        </Box>);
}
