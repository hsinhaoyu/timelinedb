import * as React from 'react';
import Box from '@mui/material/Box';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import MenuForUser from "./MenuForUser";
import MenuForEditor from "./MenuForEditor";
import NewUser from "./NewUser";

export default function App() {

    const [allTagsState, setAllTagsState] = React.useState({available: false, content: null});
    const [isLoggedIn, setIsLoggedIn] = React.useState({available: false, loggedIn: false});
    const [needsUpdate, setNeedsUpdate] = React.useState(false);

    function updateStates () {
	setNeedsUpdate(!needsUpdate);
    }

    React.useEffect( () => {
	fetch('/tags')
	    .then(res => res.json())
	    .then(data => {
		setAllTagsState({ available: true, content: data});
	    });
	
	fetch('/isLoggedin')
	    .then(res => res.json())
	    .then(data => {
		setIsLoggedIn({available: true, loggedIn: data.authenticated});
	    });
    }, [needsUpdate]);

    function mkContent() {
        const loadingBox = <Box sx={{m: 5, fontFamily: 'Special Elite', fontSize: 14}}>Loading...</Box>;

	if (isLoggedIn.available) {
	    if (isLoggedIn.loggedIn) {
	        return (<MenuForEditor
			    allTagsState = {allTagsState}
			    updateStates = {updateStates}
			    isLoggedIn = {isLoggedIn.loggedIn}
                        />);
	    } else {
	        return (<MenuForUser
			    allTagsState = {allTagsState}
			    isLoggedIn = {isLoggedIn.loggedIn}
                        />);
	    }
        } else {
	    return loadingBox;
	}
    }

    return (
        <div>
	    <BrowserRouter>
            <Routes>
                <Route path="/newUser" element={<NewUser />} />
	        <Route path="/" element={mkContent()} />
            </Routes>
            </BrowserRouter>
        </div>)
}
