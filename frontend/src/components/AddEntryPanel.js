import * as React from 'react';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormHelperText from "@mui/material/FormHelperText";
import Stack from '@mui/material/Stack';

import formatISO from 'date-fns/formatISO';
import { isValid} from 'date-fns';

import DateInput from "./DateInput";
import SingleCategoryAutocomplete from "./SingleCategoryAutocomplete";

function formatISONoTimeZone(date) {
    var str = formatISO(date);
    str = str.substring(0, str.indexOf('+'));
    return (str + "+00:00");
}

function prepareNewEntry(newEntry, includeId = true) {
    const date = formatISONoTimeZone(newEntry.date);
    const people = newEntry.people.map(item => {return item._id});
    const places = newEntry.places.map(item => {return item._id});
    const moreTags = newEntry.moreTags.map(item => {return item._id});
    const books = newEntry.books.map(item => {return item._id});
    const themes = newEntry.themes.map(item => {return item._id});
    const zz = {
	    event: newEntry.event,
	    people: people,
	    places: places,
	    moreTags: moreTags,
	    books: books,
	    themes: themes,
	    date: date
    };

    if (includeId) {
	zz["_id"] = newEntry._id;
    };
    
    return zz;
}

export default function AddEntryPanel(props) {

    function updateNewEntry(values, category) {
	props.setNewEntry(
	    prev => { return {...prev, [category]: values} });
    }

    function handleEventChange(event) {
	props.setNewEntry(
	    prev => {return {...prev, event: event.target.value}});
    }

    function handleDateChange(newValue) {
	props.setNewEntry(
	    prev => {return {...prev, date: newValue}});
    }
    
    function handleAddButtonClick() {
	if (props.newEntry.event !== "" &&
	    props.newEntry.themes.length !== 0 &&
	    props.newEntry.date !== null &&
	    isValid(props.newEntry.date)) {

	    const zz = prepareNewEntry(props.newEntry, false);

	    fetch('/new', {
		method: 'POST',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify(zz)})
            .then(res => res.json())
	    .then(res => {
	        if (res.ok) {
		    props.setNewEntry({_id: '', event: '', people: [], places: [], books: [], moreTags: [], themes: [], date: null});
                    alert("Entry successfully added");

		    // This will cause the frontend to ask the backend for tags again
		    // because new tags might have been added
		    props.updateStates();
		} else {
		    alert(res.message);
		    window.location.href="/";		    
		}
	    })
	}
    }

    function handleEditButtonClick() {
	if (props.newEntry.event !== "" &&
	    props.newEntry.themes !== [] &&
	    props.newEntry.date !== null &&
	    isValid(props.newEntry.date)) {

	    const zz = prepareNewEntry(props.newEntry);

	    fetch('/edit', {
		method: 'PUT',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify(zz)})
                .then(res => res.json())
	        .then(res => {
	        if (res.ok) {
		    props.setNewEntry({_id: '', event: '', people: [], places: [], books: [], moreTags: [], themes: [], date: null});
                    alert("Entry successfully updated");

		    // This will cause the frontend to ask the backend for tags again
		    // because new tags might have been added
		    props.updateStates();
	            // disable edit mode
	            props.setEditMode(false);
	            // switch back to the main tab
	            props.setTabState(0);
		} else {
		    alert(res.message);
		    props.setTabState(0);		    
		}
	    })
	}
    }

    function handleCancelButtonClick() {
	// first, reset the entry
	props.setNewEntry({_id: '', event: '', people: [], places: [], books: [], moreTags: [], themes: [], date: null});
	// then, disable edit mode
	props.setEditMode(false);
	// finally, switch back to the main tab
	props.setTabState(0);
    }

    function EndButton() {
	if (!props.editMode) {
	    return (
                <Button
                    sx = {{m: "15px 0px 0px 0px" , p: 0.5, width: '60px',  display: 'flex', alignSelf: 'flex-end'}}
	            disableElevation = {false}
	            variant = "contained"
                    size = "standard"
	            onClick = {handleAddButtonClick} >
	            Add
	        </Button>);
	} else {
	    return (
		<Stack spacing={2} direction="row" justifyContent = "flex-end">

                    <Button
                        sx = {{m: "15px 0px 0px 0px" , p: 0.5, width: '60px',  display: 'flex', alignSelf: 'flex-end'}}
	                disableElevation = {false}
	                variant = "contained"
                        size = "standard"
	                onClick = {handleCancelButtonClick} >
	                Cancel
	            </Button>

                    <Button
                        sx = {{m: "15px 0px 0px 0px" , p: 0.5, width: '60px',  display: 'flex', alignSelf: 'flex-end'}}
	                disableElevation = {false}
	                variant = "contained"
                        size = "standard"
	                onClick = {handleEditButtonClick} >
	                Update
	            </Button>

		</Stack>
	    )
	}
    }

    return (
	<Box sx={{width: 600}}>
            <TextField
	        multiline
                variant = "standard"
	        sx = {{ width: 600, m: 0.6, mt:5}}
	        value = {props.newEntry.event}
	        onChange = {handleEventChange}
                label = "event"
	    />
	    {props.newEntry.event === "" ?
	        <FormHelperText error margin="dense">
	            required
	        </FormHelperText> : null}
	
	    <SingleCategoryAutocomplete
	        tags = {props.allTagsState.content}
	        values = {props.newEntry.people}
	        category = 'people'
	        updateNewEntry = {updateNewEntry}
	        required = {false}
	    />

	    <SingleCategoryAutocomplete
	        tags = {props.allTagsState.content}
	        values = {props.newEntry.places}
	        category = 'places'
	        updateNewEntry = {updateNewEntry}
                required = {false}
	    />

            <SingleCategoryAutocomplete
	        tags = {props.allTagsState.content}
		values = {props.newEntry.books} 
	        category = 'books'
	        updateNewEntry = {updateNewEntry}
	        required = {false}
	    />

	    <SingleCategoryAutocomplete
	        tags = {props.allTagsState.content}
	        values = {props.newEntry.moreTags}	
	        category = 'moreTags'
	        updateNewEntry = {updateNewEntry}
	        required = {false}
	    />

	    <SingleCategoryAutocomplete
	        tags = {props.allTagsState.content}
	        values = {props.newEntry.themes}
	        category = 'themes'
	        updateNewEntry = {updateNewEntry}	
	        required = {true}
	        error = {props.newEntry.themes.length === 0}
	    />

            <DateInput
	        value = {props.newEntry.date}
	        handleDateChange = {handleDateChange}
	    />

	    <Box sx={{display: 'flex', flexDirection: 'column'}}>
	        <EndButton />
	    </Box>
	
	</Box>
    );
}
