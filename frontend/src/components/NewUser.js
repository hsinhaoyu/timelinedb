import * as React from "react";
import TextField from '@mui/material/TextField';
import Box from "@mui/material/Box";
import Button from '@mui/material/Button';

export default function NewUser() {

    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };    
    
    const handleButtonClick = (event) => {
	fetch("/register", {
	    method: "POST",
	    headers: {'Content-Type': 'application/json'},
	    body: JSON.stringify({username: email, password: password})})
	.then(res => res.json())
	.then(data => {
	    console.log(data);
	    if (data.ok === true) {
		alert("Your account was successfully created");
		window.location.href = "/";
	    } else {
		alert("Error: " + data.message);
		window.location.href = "/newUser";
	    }
	})
    }
        
    return (
	<Box component = "form"
	     noValidate
             sx = {{ml: 5, width: 400, display: 'flex', flexDirection: 'column'}}
	     autoComplete="off"
	    >

            <h1>Create a new editor account</h1>

	    <TextField
              required
	      variant = "standard"
              id = "email"
              label = "email"
	      margin = "dense"
	      value = {email}
	      onChange = {handleEmailChange}
            />

	    <TextField
              required
              variant = "standard"
              id = "password"
	      type = "password"
              label = "password"
	      margin = "dense"
	      value = {password}
	      onChange = {handlePasswordChange}
            />

            <Button
	        sx = {{m: "30px 0px 0px 0px" , p: 0.5, width: '100px',  display: 'flex', alignSelf: 'flex-end'}}
	        disableElevation = {false}
	        variant = "contained"
                size = "standard"
	        onClick = {handleButtonClick}
	    >
                Register
            </Button>
	</Box>
    );
}
