import * as React from 'react';
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Button from "@mui/material/Button";

import MainPanel from "./MainPanel";
import AboutPanel from "./AboutPanel";
import AddEntryPanel from "./AddEntryPanel";

export default function MenuForEditor(props) {
    const allTagsState = props.allTagsState;
    const [tabState, setTabState] = React.useState(0);
    const [newEntry, setNewEntry] = React.useState({_id: "", event: "", people: [], places: [], books: [], moreTags: [], themes: [], date: null});
    const [editMode, setEditMode] = React.useState(false);

    const handleTabChange = (event, newValue) => {

	// if the user wants to switch from the add/edit tab to the main tab, discard everything first before switching
	if (newValue === 0) {
	    setNewEntry({_id: "", event: "", people: [], places: [], books: [], moreTags: [], themes: [], date: null});
	    setEditMode(false);
	}
	    
        setTabState(newValue);
    };

    const ButtonInTabs = ({ onClick, children }) => {
        return <Button
	           disableRipple
	           disableElevation
	           sx={{
                      ml: 1,
                      "&.MuiButtonBase-root:hover": {
                          bgcolor: "transparent"
                      }}}
	           onClick={onClick}
	           children={children} />;
    };

    const handleLogout = () => {
	fetch("/logout")
	    .then(res => res.json())
	    .then(data => {
		// currently, the backend always return ok.
		//if (data.ok) {
		//    alert("You are logged out");
		//};
		window.location.href = "/";
	    })
    }

    return (
        <Box sx={{ width: '100%'}}>
            <Tabs
	        value = {tabState}
	        onChange = {handleTabChange}>
                <Tab disableRipple label = "Home" />
	        <Tab disableRipple label = "About" />
                <Tab disableRipple label = {editMode ? "Edit an entry" : "Add an entry"} />
                <ButtonInTabs onClick = {handleLogout}>
	            Logout
	        </ButtonInTabs>
            </Tabs>

	    <div
	        hidden = {tabState !== 0}
                id = 'pane0'>
	    
	        {tabState === 0 &&
	            (<MainPanel
		         allTagsState = {allTagsState}
		         setTabState = {setTabState}
		         setNewEntry = {setNewEntry}
		         isLoggedIn = {props.isLoggedIn} 
                         editMode = {editMode} 
                         setEditMode = {setEditMode}
		         updateStates = {props.updateStates}
		     />
		    )}
            </div>

	    <div
	        hidden = {tabState !== 1}
                id = 'pane1'>
		{tabState === 1 && (<AboutPanel />)}
            </div>

	    <div
	        hidden = {tabState !== 2}
                id = 'pane2'>
	        {tabState === 2 &&
	            (<AddEntryPanel
	                 allTagsState = {allTagsState}
		         updateStates = {props.updateStates}
		         newEntry = {newEntry}
		         setNewEntry = {setNewEntry} 
                         editMode = {editMode}
		         setEditMode = {setEditMode}
                         setTabState = {setTabState} />)}
            </div>
        </Box>
  );
}
