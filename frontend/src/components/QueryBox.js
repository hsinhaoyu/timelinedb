import * as React from 'react';
import Box from '@mui/material/Box';

import TimelineTitle from "./TimelineTitle";
import IncQueryPrompt from "./IncQueryPrompt";
import ExcQueryPrompt from "./ExcQueryPrompt";
import AutocompleteBox from "./AutocompleteBox";

function QueryBox(props) {
    return (
        <Box sx={{ p: 1, border: '1px solid', borderRadius: 2, width: '700px', display: 'flex', flexDirection: 'column'}}>

	    <TimelineTitle
	        i = {props.i}
		selTags = {props.selTags}
		setSelTags = {props.setSelTags}
	    />
	    
	    <IncQueryPrompt
	        name = "InclusionOp"
	        op = {props.selTags[props.i-1].incOp}
	        updateSelTags = {props.updateSelTags}
	        i = {props.i}
	    />

	    <AutocompleteBox
	        name = "InclusionTags"
	        allTags = {props.allTags}
		selTags = {props.selTags[props.i-1].incTags}
	        updateSelTags = {props.updateSelTags}
	        label = {props.label}
		i = {props.i}
	    />

	    <Box sx={{height: 5}}></Box>
	    
	    <ExcQueryPrompt
	        name = "ExclusionOp"
	        op = {props.selTags[props.i-1].excOp}
		updateSelTags = {props.updateSelTags}
		i = {props.i}
	    />

	    <AutocompleteBox
	        name = "ExclusionTags"
	        allTags = {props.allTags}
		selTags = {props.selTags[props.i-1].excTags}
	        updateSelTags = {props.updateSelTags}
	        label = {props.label}
		i = {props.i}
	    />

	    <Box sx={{height: 10}}></Box>

	</Box>
    )}

export default QueryBox;
