import React from "react";
import Box from '@mui/material/Box';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Stack from '@mui/material/Stack';
import { makeStyles } from '@mui/styles';
import { parseISO } from 'date-fns'

const useStyles = makeStyles((theme) => ({
    menuPaper: {
	border: 'solid 1px'
    }
}));

function handleEdit(data, setTabState, setNewEntry, setEditMode) {
    // first initialize a new entry
    setNewEntry({
	_id: data._id,
	event: data.event,
	people: data.people ? data.people.map(p => {return {_id: p, category: 'people'}}): [],
	places: data.places ? data.places.map(p => {return {_id: p, category: 'places'}}): [],
	books: data.books ? data.books.map(b => {return {_id: b, category: 'books'}}): [],
	moreTags: data.moreTags? data.moreTags.map(t => {return {_id: t, category: 'moreTags'}}): [],
	themes: data.themes ? data.themes.map(t => {return {_id: t, category: 'themes'}}): [],
	date: parseISO(data.date)
    });
    // let the edit/add item tab know that we are in edit mode
    setEditMode(true);
    // then switch to the edit/add item tab
    setTabState(2);
}

function handleDelete(data, setTabState, setEditMode, setContextMenu, handleQueryButtonClick, updateStates) {
    // close the menu
    setContextMenu(null);
    
    var r = window.confirm("Are you sure?");

    if (r === true) {
	
        fetch('/delete', {
	    method: 'DELETE',
	    headers: {'Content-Type': 'application/json'},
	    body: JSON.stringify({_id: data._id})})
	    .then(res => res.json())
            .then(res => {
	        if (res.ok) {
		    alert("Entry deleted");

	            // update the tags (because some tags might be heen removed
	            updateStates();

	           // make the same query again, update the display
	           handleQueryButtonClick();		    
	        } else {
		    alert(res.message);
	        }
	    })
    }
    // if not sure, do nothing
}

function TimelineItem(props) {
    const timelineIdx = props.timelineNo;
    const pos1 = '50px';
    const pos2 = '500px';


    const classes = useStyles();

    function TextWithMenu(props) {
	const [contextMenu, setContextMenu] = React.useState(null);

        const handleContextMenu = (event) => {
            event.preventDefault();
            setContextMenu(
                contextMenu === null
                ? {
                      mouseX: event.clientX - 2,
                      mouseY: event.clientY - 4,
                  }
               : null,
            );
        };

        const handleClose = () => {
            setContextMenu(null);
        };
	
	return (
            <div onContextMenu = {handleContextMenu} style={{ cursor: 'context-menu' }}>
	        {props.data.event}
		<Menu
                    elevation = {0}
	            classes = {{paper: classes.menuPaper}}
	            open = {contextMenu !== null}
                    onClose = {handleClose}
	            anchorReference="anchorPosition"
                    anchorPosition = {
                        contextMenu !== null
                        ? { top: contextMenu.mouseY, left: contextMenu.mouseX }
                        : undefined
                    }
                    sx = {{border: 'solid 1px'}}>
		
	            <MenuItem
	               sx = {{pl: 1, pt:0, pb:0, fontSize: 14}}
	               onClick = {(event) => {handleEdit(props.data, props.setTabState, props.setNewEntry, props.setEditMode)}} >
		       Edit
	           </MenuItem>

                   <MenuItem
                       sx = {{pl: 1, pt:0, pb: 0, fontSize: 14}}
	               onClick = {(event) => {handleDelete(props.data, props.setTabState, props.setEditMode, setContextMenu, props.handleQueryButtonClick, props.updateStates)}} >
		       Delete
	           </MenuItem>

                </Menu>

	    </div>
	)
    }
    
    
    var m = '';
    if (timelineIdx === 1) {
	m = pos1;
    } else {
	m = pos2;
    }

    return (
        <Stack
            sx = {{mt: '5px', mb: '5px', width: '1100px'}}
	    className = "timeline-item"
            alignItems = "flex-start"
	    direction = "row">
	    
	    <Box sx = {{width: '50px',  position: 'relative', left: '30px'}}>
	        {props.data.disp_year}
	    </Box>

	    <Box sx = {{width: '400px', position: 'relative', left: m}}>
	        {props.isLoggedIn
	             ? <TextWithMenu
		           data = {props.data} 
                           setTabState = {props.setTabState}
                           setNewEntry = {props.setNewEntry}
		           setEditMode = {props.setEditMode}
		           handleQueryButtonClick = {props.handleQueryButtonClick}
		           updateStates = {props.updateStates} />
                     : props.data.event}
	    </Box>

            {props.repeat ?
	         <Box sx = {{width: '400px', position: 'relative', left: '100px'}}>
	     	     {props.isLoggedIn
	                 ? <TextWithMenu
		               data = {props.data} 
                               setTabState = {props.setTabState}
                               setNewEntry = {props.setNewEntry}		      
		               setEditMode = {props.setEditMode}
		               handleQueryButtonClick = {props.handleQueryButtonClick}
		               updateStates = {props.updateStates} />		      
                         : props.data.event}
	         </Box> : null
	    }
	
        </Stack>

    )
}

export default TimelineItem;
