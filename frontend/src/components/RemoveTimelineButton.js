import * as React from 'react';

import RemoveCircleOutlineOutlinedIcon from '@mui/icons-material/RemoveCircleOutlineOutlined';
import IconButton from '@mui/material/IconButton';

// i: the index of the timeline (1 or 2)
// This button is only active when there are two timelines
function RemoveTimelineButton (props) {

    function handleClick(event) {
        var new_selTags = Array.from(props.selTags);
	new_selTags.splice(props.i - 1, 1);
	props.setSelTags(new_selTags);
    }

    return (
        <IconButton
	    aria-label = "add/delete"
	    size = "small"
	    onClick = {handleClick}>
	    
            <RemoveCircleOutlineOutlinedIcon fontSize="small" /> 

	</IconButton>
    )	    
}

export default RemoveTimelineButton;

