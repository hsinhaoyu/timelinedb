import React from "react";

function Footer() {
  const year = new Date().getFullYear();
  return (
    <footer className="footer-pin">
        <p>ⓒ Hsin-Hao Yu {year}</p>	  
    </footer>
  );
}

export default Footer;
