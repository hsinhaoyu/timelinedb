import React from "react";
import Box from '@mui/material/Box';

import TimelineItem from "./TimelineItem";

function Timeline(props) {

    const notfoundBox = <Box sx={{m: 5, fontFamily: 'Special Elite', fontSize: 14}}>No records found</Box>;

    function mkItem(item) {
	return (
	    <TimelineItem
	        key = {item._id}
	        data = {item}
	        timelineNo = {item.timelineNo}
	        repeat = {item.repeat}
                setTabState = {props.setTabState}
                setNewEntry = {props.setNewEntry}
                isLoggedIn = {props.isLoggedIn}
                editMode = {props.editMode}
                setEditMode = {props.setEditMode}
	        handleQueryButtonClick = {props.handleQueryButtonClick}
                updateStates = {props.updateStates}
	    />
	)
    }

    if (props.data.length === 0) {
	return notfoundBox;
    } else return (
        <Box sx={{ p: "50px 1px 1px 1px", width: '800px', display: 'flex', flexDirection: 'column'}}>
	    {
		props.data.map(mkItem)
	    }
	</Box>
    );
}

export default Timeline;
