import * as React from 'react';

import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import IconButton from '@mui/material/IconButton';

// Note that this button is only active when there is only one timeline
function AddTimelineButton (props) {

    function handleClick(event) {
        props.setSelTags(
	    [...props.selTags,
	     {incTags:[], excTags:[], incOp: 'any', excOp: 'any'}
	    ]);
    }

    return (
        <IconButton
	    aria-label = "add/delete"
	    size = "small"
	    onClick = {handleClick}
	  >

            <AddCircleOutlineOutlinedIcon fontSize="small" /> 

	</IconButton>
    )	    
}

export default AddTimelineButton;
