import * as React from 'react';
import Box from '@mui/material/Box';

import SiteTitle from "./SiteTitle";
import Footer from "./Footer";

export default function AboutPanel() {
    return (
        <div>
	    <div className = "content-container">
	    <SiteTitle />
	    <Box sx={{width: "700px", fontFamily: 'Times New Roman', pl:2, pr:2}}>
	    
	    <p>It is not difficult to find time lines of Beat writers on the Internet. However, most of them concentrate on the artistic careers of the Beats. This site, on the other hand, is designed mainly as a tool for readers. The emphasis is on autobiographical elements in Beat novels. You can create single time lines by querying by the name (e.g., Jack Kerouac), or by the title (e.g., On the Road). Or, you can view two time lines side-by-side. It's a useful feature for comparing the chronologies of two authors, to examine how their lives interacted (e.g., Jack Kerouac vs. William S. Burroughs). It's also useful for examining how a piece of work relates to the life of its author (e.g., On the Road vs. Jack Kerouac). I also included the dates of events on a broader scope, so that the chronology of a particular Beat author can be compared against the that of American politics (for example).</p>

	    <p>The database is neither comprehensive nor complete. The events that I chose to inlcude in the database are somewhat arbitrary. I am neither a historian nor an expert of the Beats, so you should always double-check the information presented here.</p>

	    <p>If you would like to help improve this site, please email hsinhaohh.yu@gmail.com. The source code is available at https://gitlab.com/hsinhaoyu/timelinedb</p>
	    
            </Box>
	    </div>
	    <Footer />
	</div>
    )
    
}
