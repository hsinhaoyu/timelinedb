import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import AboutDialog from "./AboutDialog";


function SiteMenu() {
    return (
	<Box className="topnav" sx={{display: 'flex', flexDirection: 'col'}}>
            <AboutDialog />

	    <Button sx = {{color: "white"}} variant = "text">
	        Login
            </Button>

	    <Button sx = {{color: "white"}} variant = "text">
	        Register
            </Button>	
        </Box>
    )
}

export default SiteMenu;
