import React from "react";

import MenuItem from "@mui/material/MenuItem";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
//import { makeStyles } from '@mui/styles';

//const useStyles = makeStyles((theme) => ({
//    rootFirstSelect: {
//	padding: "4px 0px"
//    },
//    rootSecondSelect: {
//	padding: "10px 80px"
//    }
//}));

export default function IncQueryPrompt(props) {

    //const classes = useStyles();

    return (
        <Box sx = {{display: 'flex'}}>
	
            <Box sx = {{display: 'flex', alignItems: 'center', fontSize: 14, fontFamily: 'Special Elite', p:0, m:0}}>Show entries with</Box>
	  
            <FormControl
	        variant = "standard"
	        sx = {{ml:1, p:0, minWidth: 50}}
	    >
                <Select
	            name = {props.name}

                    sx = {{fontSize: 14, fontFamily: 'Special Elite'}}
                    value = {props.op}
	            onChange = {(e, v) => {props.updateSelTags(e, v.props.value, props.i)}}
                    displayEmpty
	            disableUnderline>
		
            <MenuItem sx = {{pl:1, pt:0, pb:0, fontSize: 14, fontFamily: 'Special Elite'}} value = "all">all</MenuItem>
            <MenuItem sx = {{pl:1, pt:0, pb:0, fontSize: 14, fontFamily: 'Special Elite'}} value = "any">any</MenuItem>

                </Select>
            </FormControl>
	    
            <Box sx={{display: 'flex', alignItems: 'center', fontSize: 14, fontFamily: 'Special Elite'}}>of these tags (if blank, include everything)</Box>

        </Box>);
}
