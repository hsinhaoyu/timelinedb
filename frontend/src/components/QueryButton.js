import React from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

function QueryButton(props) {
    return (
	<Box sx = {{width: 700, display: 'flex', flexDirection: 'column'}}>
        <Button
            sx = {{m: "15px 0px 0px 0px" , p: 0.5, width: '60px',  display: 'flex', alignSelf: 'flex-end'}}
	    disableElevation = {false}
	    variant = "contained"
            size = "standard"
            onClick = {props.handleButtonClick}>
            Query
        </Button>
	</Box>
    )
}

export default QueryButton;
