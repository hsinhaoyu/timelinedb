import * as React from 'react';

function SiteTitle() {
    return (
        <div className="sitetitle">
	    <h1>An interactive Beat timeline</h1>
	    <h2>"cut world lines, cut time lines ... minutes to go" - The Soft Machine by William S. Burroughs</h2>
	</div>
    );
}

export default SiteTitle;
