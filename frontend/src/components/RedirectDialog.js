import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function RedirectDialog(props) {
    
    function handleClose() {
        props.setDialogOpen(false);
    };

    return (
        <Dialog
            open = {props.open}
            onClose = {handleClose}
	>
		
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
	            {props.message}
                </DialogContentText>
            </DialogContent>
	    
            <DialogActions>
                <Button sx={{fontFamily: "Times New Roman"}} onClick={handleClose}>Ok</Button>
            </DialogActions>
        </Dialog>);
}
