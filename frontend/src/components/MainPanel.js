import * as React from 'react';

import Box from '@mui/material/Box';

import SiteTitle from "./SiteTitle";
import QueryArea from "./QueryArea";
import Timeline from "./Timeline";
import Footer from "./Footer";

import process_timelines from "../timelineProcessing";

export default function MainPanel(props) {
    const allTagsState = props.allTagsState;
    
    const [selTags, setSelTags] = React.useState([ {incTags:[], excTags:[], incOp: 'any', excOp: 'any'}]);
    const [results, setResults] = React.useState({state: 'inactive', content: []});
    const loadingBox = <Box sx={{m: 5, fontFamily: 'Special Elite', fontSize: 14}}>Loading...</Box>;

    function handleButtonClick(event) {
	setResults({state: 'loading'});

	fetch('/query', {
	    method: 'POST',
	    headers: {'Content-Type': 'application/json'},
	    body: JSON.stringify(selTags)})
	.then(res => res.json())
        .then(data => {
	    const data_new = process_timelines(data);
	    setResults({state: 'available', content: data_new});
	})
    }

    // n is the index of the timeline (1 or 2)
    function updateSelTags(event, values, n) {
	const name = event.target.name;

	if (name === "InclusionTags") {
	    setSelTags( (prev) => {
		// make of copy of the current selected tags
		// update timeline n-1
		var new_selTags = Array.from(prev);
		new_selTags[n-1] = {...prev[n-1], incTags: values}
		return (new_selTags);
	    })
	} else if (name === "InclusionOp") {
	    setSelTags( (prev) => {
		var new_selTags = Array.from(prev);
		new_selTags[n-1] = {...prev[n-1], incOp: values}
		return (new_selTags);
	    })
	} else if (name === "ExclusionTags") {
	    setSelTags( (prev) => {
		var new_selTags = Array.from(prev);
		new_selTags[n-1] = {...prev[n-1], excTags: values}
		return (new_selTags);
	    })
        } else {
	    setSelTags( (prev) => {
		console.assert(name === "ExclusionOp");
		var new_selTags = Array.from(prev);
		new_selTags[n-1] = {...prev[n-1], excOp: values}
		return (new_selTags);
	    })		  
	}
    }
    
    function buildQueryArea() {
	return (
	    <QueryArea
	        allTags = {allTagsState.content}
	        selTags = {selTags}
	        setSelTags = {setSelTags}
                updateSelTags = {updateSelTags}
	        handleButtonClick = {handleButtonClick}
	    />
	)
    }
    
    function buildResultArea() {
        return (
	    results.state === 'loading' ? loadingBox :
		<Timeline
	            data = {results.content}
	            setTabState = {props.setTabState}
	            setNewEntry = {props.setNewEntry} 
                    isLoggedIn = {props.isLoggedIn}
                    editMode = {props.editMode}
                    setEditMode = {props.setEditMode}
	            handleQueryButtonClick = {handleButtonClick}
                    updateStates = {props.updateStates}
	    />
	)
    }

    return (
        <div>
	    <div className = "content-container">
	        <SiteTitle />
	        <span>{!allTagsState.available ? loadingBox : buildQueryArea()}</span>
	        <span>{results.state !== "inactive" ? buildResultArea() : ""}</span>
	    </div>
	    <Footer />
	</div>
    )
}
