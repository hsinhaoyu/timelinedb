import * as React from 'react';
import TextField from '@mui/material/TextField';

import DateAdapter from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import FormHelperText from "@mui/material/FormHelperText";

export default function DateInput(props) {

    return (
	<div>
        <LocalizationProvider dateAdapter = {DateAdapter}>
            <DatePicker
                label = "date"
                value = {props.value}
                onChange = {props.handleDateChange}
                renderInput = {(params) =>
	            <TextField
		        required
		        sx = {{m: 0.6}}
	                variant = "standard"
                        {...params} />}
            />
        </LocalizationProvider>
	    
	{ props.value == null ?
	  <FormHelperText error margin="dense">
	      required
	  </FormHelperText> : null
	}
	</div>    
    );
}
