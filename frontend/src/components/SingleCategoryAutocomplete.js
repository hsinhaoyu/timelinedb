import TextField from '@mui/material/TextField';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete'
import FormHelperText from "@mui/material/FormHelperText";

const filter = createFilterOptions();

// inputTags: [{_id: 'Paul Bowles', category: 'people'}, {inputValue: 'ZZZ', _id: 'add "ZZZ"'}]
// return [{_id: 'Pual Bowles'}, {_id: 'ZZZZ'}]
function simplifyTags(inputTags) {
    return inputTags.map(item => {return item.inputValue ? {_id: item.inputValue} : {_id: item._id}});
}

export default function SingleCategoryAutocomplete(props) {
    const tags = (props.tags).filter(item => {return item.category === props.category});

    const handleChange = (event, newValues) => {
	props.updateNewEntry(
	    simplifyTags(newValues),
	    props.category);
    };

    const filterOptionsF = (options, params) => {
	const filtered = filter(options, params);
	const {inputValue} = params;
	const isExisting = options.some((option) => inputValue === option.title);
	if (inputValue !== '' & !isExisting) {
	    filtered.push({
		inputValue,
		_id: `Add "${inputValue}"`,
	    });
	};
	return filtered;
    }

    const getOptionLabelF = (option) => {
	if (typeof option._id === 'string') {
          return option._id;
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue;
        }
        // Regular option
        return option._id;
    }
    
    return (
	<div>
        <Autocomplete
	    multiple
	    value = {props.values}
	    onChange = {handleChange}
	    filterOptions = {filterOptionsF}
	    selectOnFocus
            clearOnBlur
	    id = {"add-entry-" + props.category}
            options = {tags}
	    getOptionLabel = {getOptionLabelF}
	    sx = {{ width: 600, m: 0.6}}
	    freeSolo
	    size = "small"
	    renderInput = {(params) => (
                <TextField {...params}
		    variant = "standard"
		    label = {props.category} />
            )}
	/>

	{ props.required && props.values.length === 0 ?
	  <FormHelperText error margin="dense">
	      required
	  </FormHelperText> : null}

	</div>    
    )
}
