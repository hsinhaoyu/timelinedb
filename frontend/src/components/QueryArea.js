import React from "react";
import Box from '@mui/material/Box';

import QueryBox from "./QueryBox";
import QueryButton from "./QueryButton";


function QueryArea(props) {

    const n = props.selTags.length;
    
    return (
        <span>
            <QueryBox
                allTags = {props.allTags}
	        selTags = {props.selTags}
		updateSelTags = {props.updateSelTags}
	        setSelTags = {props.setSelTags}
	        label = ''
	        i = {1}
		/>

            <Box sx={{height: 20}}></Box>

	    {n === 2 ?
                <QueryBox
                    allTags = {props.allTags}
	            selTags = {props.selTags}
		    updateSelTags = {props.updateSelTags}
	            setSelTags = {props.setSelTags}
	            label = ''
	            i = {2}
		/> : null}

            <QueryButton
	        handleButtonClick = {props.handleButtonClick}
	    />

	</span>
    )
}

export default QueryArea;
