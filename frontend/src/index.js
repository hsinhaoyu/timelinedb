import * as React from 'react';
import ReactDOM from 'react-dom';

import { createTheme, ThemeProvider } from '@mui/material/styles/';
import { grey } from '@mui/material/colors';

import App from './components/App';


const theme = createTheme({
    palette: {
	primary: {
	    main: grey[800]
	}
    },
    typography: {
	fontFamily: "'Special Elite', cursive",
	fontSize: 14
    },
});


ReactDOM.render(
  <ThemeProvider theme={theme}>    
    <App />
  </ThemeProvider>,
  document.querySelector("#root")
)
