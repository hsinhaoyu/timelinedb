# An interactive Beat timeline

### About
A simple web app (written in `node.js` and `React`) that allows the users to visualize two timelines side-by-size, such that historical events that happen in parallel can be compared. The timeslines are created by querying a MongoDB database using tags. Registered editors can log in to edit the database.

This particular app is about the lives of Beat writers (see [demo](https://timelinedb-api-demo.herokuapp.com)), but the code should be general enough for other applications.

### Operations on localhost
#### To start the server:
```sh
$ npm start
```

#### To start the frontend:
```sh
$ cd frontend
$ npm start
```

#### To run unit tests
```sh
$ npm test
```

#### To run API tests
```sh
$ newman run tests-backend/API_tests.postman_collection.json -e tests/Local.postman_environment.json --folder Dev-tests
```

#### To run API tests on the deployed app
```sh
$ newman run tests-backend/API_tests.postman_collection.json -e tests/Deployment.postman_environment.json --folder Deployment-tests --env-var APP_VERSION=XXXXX
```
