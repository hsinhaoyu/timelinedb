const session = require('express-session');
const passport = require("passport");
const passportLocalMongoose = require("passport-local-mongoose");

const mongodb_access = require("./mongodb_access");

// connect to atlas, also set up authentication
function connect_atlas_with_auth(app, mongoose) {
    app.use(session({
        secret: "Our little secret",
        resave: false,
        saveUninitialized: false
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    // connect to Atlas
    mongodb_access.connect_atlas(mongoose);

    const userSchema = new mongoose.Schema({
        email: String,
        password: String
    });

    // use passportLocalMongoose to handle encryption
    userSchema.plugin(passportLocalMongoose);

    const User = new mongoose.model("User", userSchema);

    // create a local authentication strategy for passport
    passport.use(User.createStrategy());
    
    // use tell passport to use passport-local-mongoose to manage cookies
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());

    return User;
}

function isLoggedInFunc(req, res) {
    if (req.isAuthenticated()) {
	res.send({authenticated: true});
    } else {
	res.send({authenticated: false});
    }
}

function registerFunc(User, mongoose) {
    const Adminitem = mongoose.model('Adminitem', new mongoose.Schema());
    Adminitem.findOne({title: "Invited Editors"}, function(err, doc) {
	console.log(doc);
	if (err) {
	    // no invited editors found, reject all registration
	    return (req, res) => {res.status(409).send({ok: false})}
	} else {
	    const invited = doc.emails;

            return (req, res) => {
                console.log(req.body);

		if (invited.includes(req.body.username)) {
                    // user passport-local-mongoose to create a new user
                    User.register({username: req.body.username}, req.body.password, function(err, user) {
	                if (err) {
		            console.log(err);
		            res.status(409).send({ok: false});
                        } else {
	                    passport.authenticate("local")(req, res, function() {
	                        console.log('authenticated!');
		                res.send({ok: true});
			    })
		        }})
		} else {
		    // not an invited editor, reject right away
		    res.status(409).send({ok: false});
		}
	    }
	}
    })
}

function registerFunc(User, Adminitem) {
    return (req, res) => {
        Adminitem.findOne({title: "Invited Editors"}, function(err, doc) {
	    if (err) {
		// no invited editors, reject all registration
		res.status(409).send({ok: false, message: "You are not an invited editor"});
	    } else {
		const invited = doc.toObject()["emails"];
		if (invited.includes(req.body.username)) {
                    // an invited editor
                    User.register({username: req.body.username}, req.body.password, function(err, user) {
	                if (err) {
		            console.log(err);
		            res.status(409).send({ok: false, message: "You are already registered"});
                        } else {
	                    passport.authenticate("local")(req, res, function() {
		                res.send({ok: true, message: ""});
			    })
		        }});
		} else {
		    // not an invited editor, reject
		    res.status(409).send({ok: false, message: "You are not an invited editor"});
		}
	    }
	})
    }
}

function loginFunc(User) {
    return (req, res, next) => {
	const user = new User({
	    username: req.body.username,
	    password: req.body.password
	});

	passport.authenticate('local', function(err, usr, info) {
	    if (err)  { return next(err); }

	    if (!usr) {
		console.log(req.body.username + ":" + info.message);
		res.status(401).json(info);
	    } else {
		req.login(user, function() {
		    // In this case, info is "undefined"
		    console.log(user.username + " login success");
		    res.send({"message": "Login success!"});
		})
	    }
	})(req, res);		
    }
}

function logoutFunc(req, res) {
    req.logout();
    console.log("logged out");
    res.send({ok:true});
}

function createAuthRoutes(app, User, mongoose) {
    const Adminitem = mongoose.model('Adminitem', new mongoose.Schema());

    app.get("/isLoggedin", isLoggedInFunc);
    app.post("/register", registerFunc(User, Adminitem));
    app.post("/login", loginFunc(User));
    app.get("/logout", logoutFunc);
}

////////// exports
exports.connect_atlas_with_auth = connect_atlas_with_auth;
exports.createAuthRoutes = createAuthRoutes;
