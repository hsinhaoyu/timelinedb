const process = require("process");

function lastName(str) {
    return str.split(" ").slice(-1)[0];
}

function compareLastName(name1, name2) {
    const lastName1 = lastName(name1);
    const lastName2 = lastName(name2);
    return lastName1.localeCompare(lastName2);
}

function gatherByCategory(lst, category) {
    var res = lst.filter( item => (item.category === category));
    return res.map( item => item._id );
}

function gatherByCategories(lst, categories) {
    var res = {};
    categories.forEach( a => (res[a] = gatherByCategory(lst, a)) );

    // remove attribues that are []
    Object.keys(res).forEach( c => {
	if (res[c].length === 0) {
	    delete res[c];
	}
    });
    
    return res;
}


exports.lastName = lastName;
exports.compareLastName = compareLastName;
exports.gatherByCategory  = gatherByCategory;
exports.gatherByCategories = gatherByCategories;
