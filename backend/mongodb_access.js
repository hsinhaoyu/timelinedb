function readMongoDBConfig() {
    return {
	"username": process.env.MONGODB_USERNAME,
	"password": process.env.MONGODB_PASSWORD,
	"url":      process.env.MONGODB_URL,
	"db_name":  process.env.MONGODB_DB
    }
}

function mongoDBAtlasURL() {
    config = readMongoDBConfig();

    url = "mongodb+srv://"
    url = url + config.username + ":"
    url = url + config.password + "@"
    url = url + config.url + "/"
    url = url + config.db_name
    url = url + "?retryWrites=true&w=majority";

    return url;
}

function createDataModel(mongoose) {
    const entrySchema = {
	event:      String,
	people:   [ String ],
	places:   [ String ],
	moreTags: [ String ],
	themes:   [ String ],
	books:    [ String ],
	date:       Date
    };
    return mongoose.model("Item", entrySchema);
}

function connect_atlas(mongoose) {
    mongoose.connect(mongoDBAtlasURL(), {useNewUrlParser: true});
}

exports.readMongoDBConfig = readMongoDBConfig;
exports.mongoDBAtlasURL = mongoDBAtlasURL;
exports.connect_atlas = connect_atlas;
exports.createDataModel = createDataModel;
