const assert = require("assert");
const utils = require("./utils");

function wellFormedQuery(q) {
    var ok = true;
    var zz = null;
    
    try {
	// check if q is an array
        const n = q.length;

	// check if the query for each timeline has the 4 components
        incTags = q.map(item => {return item.incTags}).flat();
	excTags = q.map(item => {return item.excTags}).flat();
	incOp   = q.map(item => {return item.incOp}).flat();
	excOp   = q.map(item => {return item.excOp}).flat();

	// check if each tag has the "_id" and "category" components
        zz = incTags.map(item => {return item["_id"]});
	zz = incTags.map(item => {return item["category"]});
	
	zz = excTags.map(item => {return item["_id"]});
	zz = excTags.map(item => {return item["category"]});
	
	const opVals = ["all", "any"];
	const ops = [...incOp, ...excOp];
        const zzz = ops.map(item => {return opVals.includes(item)});

	if (!zzz.reduce((a, b) => {return a && b})) {
	    ok = false;
	}
    } catch(error) {
	ok = false;
    }
    return ok;
}

//////////////////// for routes such as "books/" or "people/"

// given an attribute (attr) such as "book", respond with all possible values
// the returned values are sorted
// Item is a mongoose model
function respondTagsForCategory(req, res, category, Item, name=false) {
    
    Item.distinct(category, (err, docs) => {
	if (!err) {
	    if (name) {
		res.send(docs.sort(utils.compareLastName));
	    } else {
		res.send(docs.sort());
	    }}
	else {
	    res.send(err);
	}
    })
}

//////////////////// for the "tags"" route

function queryTagsForPeople() {
    // people is different because we have to sort by last name
    return [
	{ $project: {people: 1, _id: 0} },
	{ $unwind: "$people" },
	{ $group: {_id: "$people",
		   lastName: {$last: {$last: {$split: ["$people", " "]}}}}
	},
	{ $addFields: {category: "people"} },
	{ $sort: {lastName: 1} },
	{ $unset: "lastName" }
    ]
}

function queryTagsForCategory_(c) {
    assert(c !== "people");

    return [
	{ $project: {[c]: 1, _id: 0} },
	{ $unwind: "$" + c},
	{ $group: {_id: "$" + c}},
	{ $addFields: {category: c} },
	{ $sort: {_id: 1} }
    ]
}

function queryTagsForCategory(c) {
    
    if (c === "people") {
	return queryTagsForPeople();
    } else {
	return queryTagsForCategory_(c);
    }
}

//////////////////// for the "query/" route


// req: {people: ['William S. Burroughs', 'Jack Kerouac'...], places: [...]}
// category: 'people'
// op: 'all'
// output: {people: {$all: ["William S. Burroughs"...]}}
function mkQForTagSet_(req, op, category, negation) {
    // This should not happen, becase gatherByCategories already filtered out empty lists
    assert(req !== []);

    if (op === 'any') {
        mongoOp = "$in";
    } else {
        mongoOp = "$all";
    }

    var res = {};
    res[category] = {};

    if (!negation) {
        res[category][mongoOp] = req[category];
    } else {
	var zz = {}
	zz[mongoOp] = req[category];
	res[category]["$not"] = zz;
    }

    return res;
}

// req: {people: ['William S. Burroughs', 'Jack Kerouac'...], places: [...]}
// op: 'all'
// output: {$and: {people: {$all: ["William S. Burroughs"...]}},
//                {places: {$all: ["..."]}}}
function mkQForTagSet(req, op, negation) {
    const categories = Object.keys(req);
    const subQs = categories.map(c => mkQForTagSet_(req, op, c, negation));

    if (op === 'any') {
        mongoOp = "$or";
    } else {
        mongoOp = "$and";
    }

    if (subQs.length === 0) {
	return {};
    } if (subQs.length === 1) {
	return subQs[0];
    } else {
	var res = {};
	res[mongoOp] = subQs;
	return res;
    }
}

function negateQ(q) {
    console.log("**", JSON.stringify(q, null, 4));
    
    var res = {};
    res['$not'] = q;
    return res;
}

function isEmpty(o){
    for (var i in o) {
        if(o.hasOwnProperty(i)){
            return false;
        }
    }
    return true;
}

// make query for a single timeline, which can have an inclusion part and an exclusion part
// req: {incTags:[....], incOp: "all", excTags: [...], excOp: "any"}
function mkQForTimeline(req, categories) {

    const incQ_ = utils.gatherByCategories(req.incTags, categories);
    const incQ = mkQForTagSet(incQ_, req.incOp, false);

    const excQ_ = utils.gatherByCategories(req.excTags, categories);
    const excQ = mkQForTagSet(excQ_, req.excOp, true);

    if (isEmpty(incQ_) && isEmpty(excQ_)) {
	return {};
    } else if (isEmpty(incQ_)) {
	return excQ;
    } else if (isEmpty(excQ_)) {
	return incQ;
    } else {
	var res = {};
	res['$and'] = [incQ, excQ];
	return res;
    }
}

// q is query returned by mkQFortimeline
// i is the index of the timeline (1, 2, 3...)
// return a mongodb aggregation pipeline
function addTimelineIndex(q, i) {
    return [
	{$match: q},
	{$addFields: {timelineNo: i}}
    ];
}

// build a mongodb aggregation pipeline for querying multiple timelines
function mkAggForTimelines(req, categories) {
    const pipelines = req.map(
	(item, i) => {
	    return addTimelineIndex(
		mkQForTimeline(item, categories), i+1)});

    if (pipelines.length === 1) {
	return [...pipelines[0], {$sort: {date: 1}}];
    } else {
	// return a new pipeline that gnereates the union on the pipelines
	const f = (q1, q2) => q1.concat([{$unionWith: {coll:'items', pipeline: q2}}]);
	const p =  pipelines.reduce(f);
	return [...p, {$sort: {date: 1, _id: 1, timelineNo: 1}}];
    }
}

exports.wellFormedQuery = wellFormedQuery;

exports.respondTagsForCategory = respondTagsForCategory;

exports.queryTagsForCategory = queryTagsForCategory;

exports.mkQForTagSet_ = mkQForTagSet_;
exports.mkQForTagSet = mkQForTagSet;
exports.mkQForTimeline = mkQForTimeline;
exports.mkAggForTimelines = mkAggForTimelines;
