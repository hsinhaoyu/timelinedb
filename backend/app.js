const fs = require("fs");
const path = require("path");

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const utils = require("./utils");
const auth = require("./auth");
const mongodb_access = require("./mongodb_access");
const mongodb_query = require("./mongodb_query");

// setup Express
const app = express();
app.use(express.static(path.resolve(__dirname, '../frontend/build')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());

// authentication
User = auth.connect_atlas_with_auth(app, mongoose);
auth.createAuthRoutes(app, User, mongoose);

// general query API

Item = mongodb_access.createDataModel(mongoose);

const categories = ['people', 'places', 'books', 'moreTags', 'themes'];

app.post("/query", (req, res) => {
    //console.log("req.body:", req.body);
    //console.log("Req.body[0]:", req.body[0]);

    const r = req.body;

    // if the request body doesn't have the correct format, return nothing
    if (!mongodb_query.wellFormedQuery(r)) {
    	res.send({});
    } else {
        const pipeline = mongodb_query.mkAggForTimelines(r, categories);
        //console.log(JSON.stringify(q, null, 4));
        Item.aggregate(
	    pipeline,
	    (err, docs) => {
		if (!err) {
	            res.send(docs);
		} else {
		    console.log("/query");
		    console.log(err);
		    res.status(500).send(err);
		}
	    })
        }
})

app.get("/tags", (req, res) => {
    const qLst = categories.map( c => mongodb_query.queryTagsForCategory(c));
    const f = (q1, q2) => q1.concat([{$unionWith: {coll:'items', pipeline: q2}}]);
    const q = qLst.reduce(f);

    Item.aggregate(
	q, 
	(err, docs) => {
	    if (!err) {
		res.send(docs);
	    } else {
		res.send(err);
	    }
	})
})

categories.forEach(category => {
    app.get("/" + category, (req, res) => {
	if (category === "people") {
	    mongodb_query.respondTagsForCategory(req, res, category, Item, name=true);
	} else {
	    mongodb_query.respondTagsForCategory(req, res, category, Item);
	}
    })
})	    

app.get("/info", (req, res) => {
    const v = JSON.parse(fs.readFileSync('version.json'));
    res.send(v);
})

// Editor API

app.post("/new", (req, res) => {
    if (req.isAuthenticated()) {
        const r = req.body;
        const newItem = new Item(r);
    
        newItem.save(err => {
	    if (!err) {
		//console.log('added');
	        res.send({ok: true});
	    } else {
		console.log("/new");
		console.log(err);
	        res.status(500).send({ok: false, message: err});
	    }
        });
    } else {
	res.status(401).send({ok: false, message: "Can't add an entry without logging in"});
    }
})

app.put("/edit", (req, res) => {
    if (req.isAuthenticated()) {
	const r = req.body;
	Item.findByIdAndUpdate(
	    r._id,
	    r,
	    {overwrite: true},
	    function (err) {
		if (!err) {
		    res.send({ok: true, message: "Success"});
		} else {
		    console.log("/edit");
		    console.log(err);
		    res.status(500).send({ok: false, message: err});
		} 
	    });
    } else {
	res.status(401).send({ok: false, message: "Can't edit without logging in"});
    }
})

app.delete("/delete", (req, res) => {
    if (req.isAuthenticated()) {
	const r = req.body;
	Item.findByIdAndDelete(
	    r._id,
	    function (err) {
		if (!err) {
		    res.send({ok: true, message: "Success"});
		} else {
		    console.log("/delete");
		    console.log()
		    res.status(500).send({ok: false, message: err});
		} 
	    });
    } else {
	res.status(401).send({ok: false, message: "Can't delete without logging in"});
    }
})

// All other GET requests will return the React frontend
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../frontend/build', 'index.html'))
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, function () {
    console.log(`Server started on ${PORT}`);
});
