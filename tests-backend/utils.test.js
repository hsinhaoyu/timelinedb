const utils = require('../backend/utils')
const mongodb_access = require('../backend/mongodb_access.js');

test("test lastName()", async () => {
    expect(utils.lastName("Jack Kerouac")).toBe("Kerouac");
})

test("Environment variables are defined for MongoDB", async () => {
    config = mongodb_access.readMongoDBConfig();
    expect(config.username).toBeDefined();
    expect(config.password).toBeDefined();
    expect(config.url).toBeDefined();
    expect(config.db_name).toBeDefined();
})

test("gatherByCategory()", async () => {
    input = [
	{ _id: 'Jack Kerouac', category: 'people' },
        { _id: 'Allen Ginsberg', category: 'people' },	
        { _id: 'Mexico', category: 'places' }];
    expectation1 = ["Jack Kerouac", "Allen Ginsberg"];
    expectation2 = ["Mexico"];

    out1 = utils.gatherByCategory(input, "people");
    out2 = utils.gatherByCategory(input, "places");
    
    expect(out1).toEqual(expect.arrayContaining(expectation1));
    expect(expectation1).toEqual(expect.arrayContaining(out1));

    expect(out2).toEqual(expect.arrayContaining(expectation2));
    expect(expectation2).toEqual(expect.arrayContaining(out2));    
})

test("gatherByCategory() edge cases", async () => {
    const out1 = utils.gatherByCategory([], "people");
    expect(out1).toEqual([]);

    const input = [["people", "Jack Kerouac"], ["people", "Allen Ginsberg"], ["places", "Mexico"]];
    const out2 = utils.gatherByCategory(input, "tags");
    expect(out2).toEqual([]);
})

test("gatherByCategories()", async () => {
    input = [
	{ _id: 'Jack Kerouac', category: 'people' },
        { _id: 'Allen Ginsberg', category: 'people' },	
        { _id: 'Mexico', category: 'places' }];    
    expectation1 = ["Jack Kerouac", "Allen Ginsberg"];
    expectation2 = ["Mexico"];

    out  = utils.gatherByCategories(input, ["people", "places", "moreTags"]);
    out1 = out.people;
    out2 = out.places;
    out3 = out.moreTags;

    expect(out1).toEqual(expect.arrayContaining(expectation1));
    expect(expectation1).toEqual(expect.arrayContaining(out1));

    expect(out2).toEqual(expect.arrayContaining(expectation2));
    expect(expectation2).toEqual(expect.arrayContaining(out2));

    // make sure that there is no "tag" attribute
    expect(out3).toBeUndefined();
})

test("gatherByCategories() edge cases", async () => {
    out = utils.gatherByCategories([], ["people", "places", "moreTags"]);
    expect(out).toEqual({});
})
