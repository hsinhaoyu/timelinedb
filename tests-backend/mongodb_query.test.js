const mongodb_query = require('../backend/mongodb_query');

// test("is query well formed?", () => {
//     q = [];
//     ok = mongodb_query.checkQueryOk(q);
//     expect(ok).toBe(false);

//     q0 = {incTags: [],
// 	  excTags: [],
//           incOp: 'any',
//           excOp: 'any'};
    

//     q1 = {incTags: [ { _id: 'Paul Bowles', category: 'people' } ],
// 	  excTags: [ { _id: 'William S. Burroughs', category: 'people' } ],
//           incOp: 'any',
//           excOp: 'any'};


//     q = [q1, q1];
//     ok = mongodb_query.checkQueryOk(q);
//     expect(ok).toBe(true);

//     // cannot have just one query. Must have two
//     q = [q1];
//     ok = mongodb_query.checkQueryOk(q);
//     expect(ok).toBe(false);

//     // the second query can be empty
//     q = [q1, q0];
//     ok = mongodb_query.checkQueryOk(q);
//     expect(ok).toBe(true);    

    
// })

test("mkQForTagSet_(), any", () => {
    input = {people: ["Jack Kerouac", "Allen Ginsberg"],
	     places: ["Mexico"]};

    // expected:
    //     {people: {"$in": ["Jack Kerouac", "Allen Ginsberg"]}}
    out = mongodb_query.mkQForTagSet_(input, "any", "people");

    expect(Object.keys(out).length).toBe(1);
    expect(Object.keys(out)[0]).toEqual("people");

    expect(Object.keys(out["people"]).length).toBe(1);
    expect(Object.keys(out["people"])[0]).toEqual("$in");

    expect(out["people"]["$in"].length).toBe(2);
    expect(out["people"]["$in"][0]).toEqual("Jack Kerouac");
    expect(out["people"]["$in"][1]).toEqual("Allen Ginsberg");
})

test("mkQForTagSet(), any", () => {
   input = {people: ["Jack Kerouac", "Allen Ginsberg"],
	    place:  ["Mexico"]};

    // expected:
    // {$or: [ {people: {$in: ["Jack", "Allen"]},
    //         {place:  {$in: ["Mexico"]}}
    //        ]}
    out = mongodb_query.mkQForTagSet(input, "any");

    expect(Object.keys(out).length).toBe(1);
    expect(out).not.toBeUndefined();

    // should be ['people','place']
    out3 = out["$or"].map(item => (Object.keys(item))).flat().sort();
    expect(out3.length).toBe(2);
    expect(out3[0]).toEqual('people');
    expect(out3[1]).toEqual('place');
})

test("mkQForTagSet(), edge case", () => {
    input = {};
    out = mongodb_query.mkQForTagSet(input, "any");
    expect(out).toEqual({});
})

test("mkQForTimeline()", async () => {
    incTags0 = [
	{ _id: 'Jack Kerouac', category: 'people' },
        { _id: 'Allen Ginsberg', category: 'people' }];
    
    excTags0 = [ { _id: 'Mexico', category: 'places' }];
    
    input = {incTags: incTags0, incOp: 'any',
	     excTags: excTags0, excOp: 'any'};

    out = mongodb_query.mkQForTimeline(input, ["people", "places", "themes"]);

    // expection:  { '$and': [ { people: [Object] }, { places: {$not: {...}} } ] }
    expect(out['$and'].length).toBe(2);
    expect(out['$and'][0].people['$in'].length).toBe(2);
    expect(out['$and'][1].places['$not']['$in'][0]).toEqual('Mexico');
})

test("mkQForTimeline(), inc empty", async () => {
    incTags0 = [];
    excTags0 = [ { _id: 'Mexico', category: 'places' }];
    
    input = {incTags: incTags0, incOp: 'any',
	     excTags: excTags0, excOp: 'any'};

    out = mongodb_query.mkQForTimeline(input, ["people", "places", "themes"]);
    console.log(out);

    // expection: { places: { $not: {$in: [Array] } } }
    expect(out.places['$not']['$in'][0]).toEqual('Mexico');
})

test("mkQForTimeline() exc empty", async () => {
    incTags0 = [
	{ _id: 'Jack Kerouac', category: 'people' },
        { _id: 'Allen Ginsberg', category: 'people' }];
    
    excTags0 = [];
    
    input = {incTags: incTags0, incOp: 'any',
	     excTags: excTags0, excOp: 'any'};

    out = mongodb_query.mkQForTimeline(input, ["people", "places", "themes"]);

    // expection: { people: { '$in': [ 'Jack Kerouac', 'Allen Ginsberg' ] } }
    expect(out['people']['$in'].length).toBe(2);
})

test("mkQForTimeline() both empty", async () => {
    incTags0 = []
    excTags0 = [];
    
    input = {incTags: incTags0, incOp: 'any',
	     excTags: excTags0, excOp: 'any'};

    out = mongodb_query.mkQForTimeline(input, ["people", "places", "themes"]);

    // expection: {}
    expect(out).toMatchObject({});
})
